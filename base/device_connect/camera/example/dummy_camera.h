/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include "base/device_connect/camera/device_base.h"

#include <map>
#include <string>

namespace os {
namespace v2x {
namespace device {

/**
 * @brief 演示如何注册一个相机设备到相机工厂
 *
 */
class DummyCamera : public CameraDevice {
 public:
  explicit DummyCamera(const CameraStreamCallBack& cb) : CameraDevice{cb} {}
  virtual ~DummyCamera() = default;

  bool Init(const std::string& config_file) override;

  std::shared_ptr<CameraData> GetImage(const std::string& camera_name) override;

  CameraDeviceState GetState(const std::string& camera_name) override;

 private:
  std::map<std::string, int> camera_map_;
};

}  // namespace device
}  // namespace v2x
}  // namespace os
