/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include "air_middleware_component.h"
#include "base/device_connect/proto/cloud_data.pb.h"
#include "middleware/device_service/proto/traffic_light_service.pb.h"
#include "middleware/protocol/proto/v2xpb-asn-message-frame.pb.h"

namespace os {
namespace v2x {
namespace protocol {

class AIROS_COMPONENT_CLASS_NAME(TrafficLightAdapterComponent)
    : public airos::middleware::ComponentAdapter<
          os::v2x::TrafficLightServiceData> {
 public:
  AIROS_COMPONENT_CLASS_NAME(TrafficLightAdapterComponent)() : spat_count_(1){};

  virtual ~AIROS_COMPONENT_CLASS_NAME(
      TrafficLightAdapterComponent)() override{};
  bool Init() override;
  bool Proc(const std::shared_ptr<const os::v2x::TrafficLightServiceData>&
                service_data) override;

 private:
  bool TrafficLightServicePb2Cloud(
      const std::shared_ptr<const os::v2x::TrafficLightServiceData>&
          service_data,
      std::shared_ptr<os::v2x::device::CloudData>& cloud_pb);
  bool TrafficLightServicePb2AsnPb(
      const std::shared_ptr<const os::v2x::TrafficLightServiceData>&
          service_data,
      std::shared_ptr<v2xpb::asn::MessageFrame>& asn_pb);
  int64_t get_minute_year();
  int64_t get_mill_second_minute();
  bool send_step_control(int step);
  
  int spat_count_;
  // max PhaseID 255
  static const uint32_t MAX_VEHICLE_PHASE = 200;
  std::string rscu_sn_;
  std::string MQTT_TRAFFICLIGHT_TOPIC_PREFIX = "upload/trafficlight/";
};

REGISTER_AIROS_COMPONENT_CLASS(TrafficLightAdapterComponent,
                               os::v2x::TrafficLightServiceData);

}  // namespace protocol
}  // namespace v2x
}  // namespace os
