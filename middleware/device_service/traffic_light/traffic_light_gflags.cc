/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "traffic_light_gflags.h"

namespace os {
namespace v2x {
namespace service {

DEFINE_string(city_code, "guangzhou", "city code for traffic light");
DEFINE_int32(region_id, 44, "region id in spat");
DEFINE_int32(cross_id, 100, "intersection id, node id in spat");
DEFINE_string(phase_to_direction_path, "phase_to_direction.json",
              "signal machine phase trans to custom phase");

}  // namespace service
}  // namespace v2x
}  // namespace os