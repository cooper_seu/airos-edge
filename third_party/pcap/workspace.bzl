def clean_dep(dep):
    return str(Label(dep))

def repo():
    native.new_local_repository(
        name = "pcap",
        build_file = clean_dep("//third_party/pcap:pcap.BUILD"),
        path = "/usr"
    )