def clean_dep(dep):
    return str(Label(dep))

def repo():
    native.new_local_repository(
        name = "eigen",
        build_file = clean_dep("//third_party/eigen:eigen.BUILD"),
        path = "/usr/include/eigen3"
    )