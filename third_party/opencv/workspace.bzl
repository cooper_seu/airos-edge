def clean_dep(dep):
    return str(Label(dep))

def repo():
    native.new_local_repository(
        name = "opencv",
        build_file = clean_dep("//third_party/opencv:opencv.BUILD"),
        path = "/opt/opencv4.2"
    )