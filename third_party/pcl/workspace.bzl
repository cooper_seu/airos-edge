def clean_dep(dep):
    return str(Label(dep))

def repo():
    native.new_local_repository(
        name = "pcl",
        build_file = clean_dep("//third_party/pcl:pcl.BUILD"),
        path = "/usr/include/pcl-1.10"
    )